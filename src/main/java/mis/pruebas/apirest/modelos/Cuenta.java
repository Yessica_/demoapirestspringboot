package mis.pruebas.apirest.modelos;

public class Cuenta {
    public String numero;
    public String moneda;
    public double saldo;
    public String tipo;
    public String estado;
    public String oficina;
    public String documento; //Atributo agregado para relacionar clientes con su respectiva cuenta.
}

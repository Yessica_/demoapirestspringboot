package mis.pruebas.apirest.servicios.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;

@Service
public class ServicioClienteImpl implements ServicioCliente {
    
	@Autowired
    ServicioCuenta servicioCuenta;

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();


    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        List<Cliente> clientesContados = List.copyOf(this.clientes.values());
        int indiceInicial = pagina*cantidad;
        int indiceFinal = indiceInicial+cantidad;
        if (indiceFinal > clientesContados.size()){
            indiceFinal= clientesContados.size();
        }
        
        List<Cliente> listado=clientesContados.subList(indiceInicial , indiceFinal);
        if(listado!=null) {
        	System.out.println("listado obtener cuentas");
        	for (Cliente objeto : listado) {
        		try {
            		List<Cuenta> listCuentas=servicioCuenta.obtenerCuentasCliente(objeto.documento);
            		if(listCuentas!=null) {
            			objeto.cuentas=new ArrayList<>();
            			objeto.cuentas.addAll(listCuentas);
            		}
				} catch (Exception e) {
					System.out.println("error cuenta :"+e);
				}
        		
			}
        }
        return listado;
    }


        @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento,cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        Cliente objeto=null;
        if (this.clientes.containsKey(documento)){
            objeto= this.clientes.get(documento);
            
            List<Cuenta> listCuentas=servicioCuenta.obtenerCuentasCliente(objeto.documento);
            if(listCuentas!=null) {
            	objeto.cuentas=new ArrayList<>();
            	objeto.cuentas.addAll(listCuentas);
            }
        }
        return objeto;
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if(!this.clientes.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente" + cliente.documento);

        //"Piso" el objeto en el hashmap
        this.clientes.replace(cliente.documento,cliente);

    }
    @Override
    public void emparcharCliente(Cliente parche){
        final Cliente existente=this.clientes.get(parche.documento);

        if(parche.edad != null)
            existente.edad=parche.edad;

        if(parche.nombre != null)
            existente.nombre=parche.nombre;

        if(parche.correo != null)
            existente.correo=parche.correo;

        if(parche.direccion != null)
            existente.direccion=parche.direccion;

        if(parche.telefono != null)
            existente.telefono=parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento=parche.fechaNacimiento;

        this.clientes.replace(existente.documento,existente);
    }

    @Override
    public void borrarCliente(String documento) {
        this.clientes.remove(documento);
        //remover el cliente de las cuentas
        servicioCuenta.borrarClienteCuentas(documento);
        
    }

    @Override
    public void agregarCuentaCliente(String documento, Cuenta cuenta){
        Cuenta objeto=servicioCuenta.obtenerCuenta(cuenta.numero);
        objeto.documento=documento;
        servicioCuenta.emparcharCuenta(objeto);
      /*  final Cliente cliente =this.obtenerCliente(documento);
        cliente.cuentas.add(cuenta);*/
    }

    @Override
    public List<Cuenta> obtenerCuentasCliente(String documento){
        final Cliente cliente=this.obtenerCliente(documento);
        return cliente.cuentas;
    }
    @Override
    public void reemplazarCuentasCliente(String documento, Cuenta cuenta){
    	cuenta.documento=documento;
        servicioCuenta.guardarCuenta(cuenta);
    }

    @Override
    public void emparcharCuentaCliente(String documento, Cuenta cuenta){
    	cuenta.documento=documento;
        servicioCuenta.emparcharCuenta(cuenta);
    }
    @Override
    public void deleteCuentasCliente(String documento, Cuenta cuenta){
        final Cliente cliente=this.obtenerCliente(documento);
        if(cliente!=null){
            for (int i = 0; i <cliente.cuentas.size(); i++) {
                if(cliente.cuentas.get(i).numero.equals(cuenta.numero)) {
                    cliente.cuentas.remove(i);
                    break;
                }

            }
            this.clientes.replace(cliente.documento,cliente);

        }
    }


   }

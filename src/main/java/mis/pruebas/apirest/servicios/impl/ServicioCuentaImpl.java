package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {
    public final Map<String, Cuenta> cuenta =new ConcurrentHashMap<>();

    public List<Cuenta> obtenerCuentas(int pagina, int cantidad){
        List<Cuenta> cuentasContados = List.copyOf(this.cuenta.values());
        int indiceInicial = pagina*cantidad;
        int indiceFinal = indiceInicial+cantidad;
        if (indiceFinal > cuentasContados.size()){
            indiceFinal= cuentasContados.size();
        }
        return cuentasContados.subList(indiceInicial , indiceFinal);
    }

    @Override
    public void insertarCuentaNuevo(Cuenta cuenta) {
        this.cuenta.put(cuenta.numero,cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
         Cuenta objeto=null;
        if (this.cuenta.containsKey(numero)){
            objeto= this.cuenta.get(numero);
        }
        return objeto;
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if(!this.cuenta.containsKey(cuenta.numero))
            throw new RuntimeException("No existe la cuenta" + cuenta.numero);
        //"Piso" el objeto en el hashmap
        this.cuenta.replace(cuenta.numero,cuenta);


    }
    @Override
    public void emparcharCuenta(Cuenta parche){
        final Cuenta existente=this.cuenta.get(parche.numero);

        if(parche.moneda != null)
            existente.moneda=parche.moneda;

        if(parche.saldo != 0)
            existente.saldo=parche.saldo;

        if(parche.tipo != null)
            existente.tipo=parche.tipo;

        if(parche.estado != null)
            existente.estado=parche.estado;

        if(parche.oficina != null)
            existente.oficina=parche.oficina;

        if(parche.documento!=null)
            existente.documento=parche.documento;

        this.cuenta.replace(existente.numero,existente);

    }

    @Override
    public void borrarCuenta(String numero) {
        this.cuenta.remove(numero);
    }

    @Override
    public  List<Cuenta> obtenerCuentasCliente (String documento){
        List<Cuenta> listado=null;
        if(this.cuenta.size()>0) {
	        for (Map.Entry valor : this.cuenta.entrySet()) {
	            Cuenta objeto=(Cuenta) valor.getValue();
	            if(objeto!=null && objeto.documento!=null && objeto.documento.equals(documento)){
	                if(listado==null)
	                    listado=new ArrayList<Cuenta>();
	
	                listado.add(objeto);
	            }
	        }
        }
        return listado;
    }

	@Override
	public void borrarClienteCuentas(String documento) {
		for (Map.Entry valor : this.cuenta.entrySet()) {
            Cuenta objeto=(Cuenta) valor.getValue();
            if(objeto!=null && objeto.documento!=null && objeto.documento.equals(documento)){
            	this.cuenta.get(valor.getKey()).documento=null;
            }
        }
		
	}

}

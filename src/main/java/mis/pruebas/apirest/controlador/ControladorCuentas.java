package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuentas {
    @Autowired(required=true)
    ServicioCuenta servicioCuenta;

    //GET http://localhost:8080/api/vl/cuenta -->List<Cuenta> obtenerCuenta()
    //@RequestMapping(method = RequestMethod.GET)


   @GetMapping
    public List<Cuenta> obtenerCuenta(@RequestParam int pagina, @RequestParam int cantidad){
       try {
           return this.servicioCuenta.obtenerCuentas(pagina - 1, cantidad);
       } catch(Exception x) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND);
       }
    }


    //POST http://localhost:8080/api/vl/cuenta + DATOS
    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta cuenta){
       this.servicioCuenta.insertarCuentaNuevo(cuenta);
        return new ResponseEntity<>("Cuenta creada correctamente!", HttpStatus.CREATED);
    }

    //**************************************************//

    // GET http://localhost:8080/api/vl/cuenta/{cuenta}    -->obtenerUnaCuenta(numero)
    // GET http://localhost:8080/api/vl/cuenta/12345678     -->obtenerUnaCuenta("12345678")
    @GetMapping("/{numero}")
    public ResponseEntity obtenerUnaCuenta(@PathVariable String numero){
        Cuenta objeto = this.servicioCuenta.obtenerCuenta(numero);
        if (objeto == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(objeto);

    }

    // PUT http://localhost:8080/api/vl/cuentas/{numero} + DATOS   -->reemplazarUnaCuenta(documento)
    // PUT http://localhost:8080/api/vl/clientes/12345678 +DATOS    -->reemplazarUnaCuenta("12345678")
    @PutMapping("/{numero}")
    public ResponseEntity reemplazarUnaCuenta(@PathVariable("numero") String nroCuenta,
                                    @RequestBody Cuenta cuenta){
        cuenta.numero=nroCuenta;
        Cuenta objeto= this.servicioCuenta.obtenerCuenta(nroCuenta);
        if (objeto==null){
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.servicioCuenta.guardarCuenta(cuenta);
        return new ResponseEntity<>("Cuenta actualizada correctamente.", HttpStatus.OK);
    }



    // PUT http://localhost:8080/api/vl/cuenta/{numero} + DATOS   -->emparcharUnaCuenta(numero)
    // PUT http://localhost:8080/api/vl/cuenta/12345678 +DATOS    -->emparcharUnaCuenta("12345678")
    @PatchMapping("/{numero}")
    public ResponseEntity emparcharUnaCuenta(@PathVariable("numero") String nroCuenta,
                                   @RequestBody Cuenta cuenta){
        cuenta.numero=nroCuenta;
        Cuenta objeto = this.servicioCuenta.obtenerCuenta(nroCuenta);
        if (objeto == null) {
            return new ResponseEntity<>("Cuenta no encontrada.", HttpStatus.NOT_FOUND);
        }
        this.servicioCuenta.emparcharCuenta(cuenta);
        return new ResponseEntity<>("Cuenta actualizada correctamente.", HttpStatus.OK);
    }

    // DELETE http://localhost:8080/api/vl/clientes/12345678
    @DeleteMapping("/{numero}")
    public ResponseEntity EliminarUnCliente(@PathVariable("numero") String nroCuenta){
        Cuenta objeto = this.servicioCuenta.obtenerCuenta(nroCuenta);
        if(objeto == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.servicioCuenta.borrarCuenta(nroCuenta);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
    }


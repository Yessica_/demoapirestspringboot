package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    //GET http://localhost:8080/api/vl/clientes -->List<Cliente> obtenerClientes()
    //@RequestMapping(method = RequestMethod.GET)

    @GetMapping
    public List<Cliente> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioCliente.obtenerClientes(pagina - 1, cantidad);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //POST http://localhost:8080/api/vl/clientes + DATOS
    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente){
        this.servicioCliente.insertarClienteNuevo(cliente);
        return new ResponseEntity<>("Cliente creado correctamente!", HttpStatus.CREATED);
    }

    // GET http://localhost:8080/api/vl/clientes/{documento}    -->obtenerUnCliente(documento)
    // GET http://localhost:8080/api/vl/clientes/12345678     -->obtenerUnCliente("12345678")
   @GetMapping("/{documento}")
    public ResponseEntity obtenerUnCliente(@PathVariable String documento) {
        Cliente objeto = this.servicioCliente.obtenerCliente(documento);
        if (objeto == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(objeto);
    }


    // PUT http://localhost:8080/api/vl/clientes/{documento} + DATOS   -->reemplazarUnCliente(documento)
    // PUT http://localhost:8080/api/vl/clientes/12345678 +DATOS    -->reemplazarUnCliente("12345678")
    @PutMapping("/{documento}")
    public ResponseEntity reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                              @RequestBody Cliente cliente){
        cliente.documento=nroDocumento;
        Cliente objeto= this.servicioCliente.obtenerCliente(nroDocumento);
        if (objeto==null){
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.servicioCliente.guardarCliente(cliente);
        return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
    }

    // PUT http://localhost:8080/api/vl/clientes/{documento} + DATOS   -->emparcharUnCliente(documento)
    // PUT http://localhost:8080/api/vl/clientes/12345678 +DATOS    -->emparcharUnCliente("12345678")
   @PatchMapping("/{documento}")
    public ResponseEntity emparcharUnCliente(@PathVariable("documento") String nroDocumento,
                                   @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        Cliente objeto = this.servicioCliente.obtenerCliente(nroDocumento);
        if (objeto == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.servicioCliente.emparcharCliente(cliente);
        return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
    }


    // DELETE http://localhost:8080/api/vl/clientes/12345678

    @DeleteMapping("/{documento}")
    public ResponseEntity EliminarUnCliente(@PathVariable("documento") String nroDocumento){
        Cliente objeto = this.servicioCliente.obtenerCliente(nroDocumento);
        if(objeto == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.servicioCliente.borrarCliente(nroDocumento);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}



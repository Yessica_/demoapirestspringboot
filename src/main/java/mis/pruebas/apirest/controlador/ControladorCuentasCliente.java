package mis.pruebas.apirest.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {
    //CRUD - GET, GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCuenta servicioCuenta;


    //POST http://localhost:8080/api/v1/clientes/12345678/cuentas + DATOS -->agregarCuentaCliente(documento,cliente)
    @PostMapping
    public ResponseEntity  agregarCuentaCliente(@PathVariable String documento,
                                      @RequestBody Cuenta cuenta){
        try {
            this.servicioCliente.agregarCuentaCliente(documento, cuenta);
        } catch(Exception x){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuentasCliente(@PathVariable String documento){
        try {
             final List<Cuenta> cuentasCliente=this.servicioCuenta.obtenerCuentasCliente(documento);
             return ResponseEntity.ok(cuentasCliente);
        } catch (Exception x){
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public ResponseEntity  reemplazarCuentaCliente(@PathVariable String documento,
                                                @RequestBody Cuenta cuenta){
        try {
            this.servicioCliente.reemplazarCuentasCliente(documento, cuenta);
        } catch(Exception x){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @PatchMapping
    public ResponseEntity  emparcharCuentaCliente(@PathVariable String documento,
                                                   @RequestBody Cuenta cuenta){
        try {
            this.servicioCliente.emparcharCuentaCliente(documento, cuenta);
        } catch(Exception x){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity  deleteCuentasCliente(@PathVariable String documento,
                                                  @RequestBody Cuenta cuenta){
        try {
            this.servicioCliente.deleteCuentasCliente(documento, cuenta);
        } catch(Exception x){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
